using System;
using System.Collections;
using UnityEngine;

public class CoroutineUtil : MonoBehaviour
{
    public static CoroutineUtil Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void WaitForEndOfFrame(Action action)
    {
        StartCoroutine(WaitForEndOfFrameCoroutine(action));
    }

    private IEnumerator WaitForEndOfFrameCoroutine(Action action)
    {
        yield return new WaitForEndOfFrame();
        action();
    }
}
