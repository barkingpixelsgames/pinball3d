using System;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static event Action<int> OnScoreChanged;
    
    private int _currentScore;

    private void OnEnable()
    {
        Mission.OnMissionComplete += OnMissionComplete;
        UiManager.OnGameReset += OnGameReset;
    }

    private void OnDisable()
    {
        Mission.OnMissionComplete -= OnMissionComplete;
        UiManager.OnGameReset -= OnGameReset;
    }

    private void Start()
    {
        AddScore(0);
    }

    private void OnMissionComplete(int missionId, int score)
    {
        AddScore(score);
    }

    private void AddScore(int scoreToAdd)
    {
        _currentScore += scoreToAdd;
        OnScoreChanged?.Invoke(_currentScore);
    }

    private void OnGameReset()
    {
        _currentScore = 0;
        AddScore(0);
    }
}
