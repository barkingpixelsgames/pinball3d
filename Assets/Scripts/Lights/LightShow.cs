using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LightModes
{
    Single,
    Airplane,
    PingPong,
    AllAtOnce
}

public class LightShow : MonoBehaviour
{
    [SerializeField] private float blinkInterval = 0.5f;
    [SerializeField] private LightModes lightMode;
    [SerializeField] private List<LightSet> lightSetList = new List<LightSet>();
    
    private bool _isLightShowRunning;
    private int _lightIndex;
    private int _direction = 1;
    
    public void StartLightShow()
    {
        StartCoroutine(Blink());
    }

    public void StopLightShow()
    {
        StartCoroutine(StopBlink());
    }

    private IEnumerator Blink()
    {
        if (_isLightShowRunning)
        {
            yield break;
        }

        yield return new WaitForSeconds(blinkInterval * 2);
        _isLightShowRunning = true;

        while (_isLightShowRunning)
        {
            if (lightMode == LightModes.Single)
            {
                // Set sprite on
                lightSetList[0].GetRenderer.sprite = lightSetList[0].GetOnSprite;
                // Wait
                yield return new WaitForSeconds(blinkInterval);
                // Set sprite off
                lightSetList[0].GetRenderer.sprite = lightSetList[0].GetOffSprite;
                // Wait
                yield return new WaitForSeconds(blinkInterval);
            }
            
            if (lightMode == LightModes.Airplane)
            {
                // Set sprite on
                lightSetList[_lightIndex].GetRenderer.sprite = lightSetList[_lightIndex].GetOnSprite;
                // Wait
                yield return new WaitForSeconds(blinkInterval);
                // Set sprite off
                lightSetList[_lightIndex].GetRenderer.sprite = lightSetList[_lightIndex].GetOffSprite;
                // Wait
                yield return new WaitForSeconds(blinkInterval);
                
                _lightIndex++;
                if (_lightIndex > lightSetList.Count - 1)
                {
                    _lightIndex = 0;
                }
            }
            
            if (lightMode == LightModes.PingPong)
            {
                // Set sprite on
                lightSetList[_lightIndex].GetRenderer.sprite = lightSetList[_lightIndex].GetOnSprite;
                // Wait
                yield return new WaitForSeconds(blinkInterval);
                // Set sprite off
                lightSetList[_lightIndex].GetRenderer.sprite = lightSetList[_lightIndex].GetOffSprite;
                // Wait
                yield return new WaitForSeconds(blinkInterval);

                _lightIndex += 1 * _direction;
                if (_lightIndex > lightSetList.Count - 1)
                {
                    _lightIndex = lightSetList.Count - 1;
                    _direction = -1;
                }
                else if (_lightIndex < 0)
                {
                    _lightIndex = 0;
                    _direction = 1;
                }
            }
            
            if (lightMode == LightModes.AllAtOnce)
            {
                // Set sprite on
                foreach (var lightSet in lightSetList)
                {
                    lightSet.GetRenderer.sprite = lightSet.GetOnSprite;
                }
                // Wait
                yield return new WaitForSeconds(blinkInterval);
                // Set sprite off
                foreach (var lightSet in lightSetList)
                {
                    lightSet.GetRenderer.sprite = lightSet.GetOffSprite;
                }
                // Wait
                yield return new WaitForSeconds(blinkInterval);
            }
        }
    }

    private IEnumerator StopBlink()
    {
        _isLightShowRunning = false;
        yield return new WaitForSeconds(blinkInterval * 2);

        foreach (var lightSet in lightSetList)
        {
            lightSet.GetRenderer.sprite = lightSet.GetOffSprite;
        }
    }
}

[Serializable]
public class LightSet
{
    [SerializeField] private SpriteRenderer renderer;
    [SerializeField] private Sprite onSprite;
    [SerializeField] private Sprite offSprite;

    public SpriteRenderer GetRenderer => renderer;
    public Sprite GetOnSprite => onSprite;
    public Sprite GetOffSprite => offSprite;
}
