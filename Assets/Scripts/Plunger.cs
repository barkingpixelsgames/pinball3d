using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Plunger : MonoBehaviour
{
    [SerializeField] private float maxPower = 2;
    [SerializeField] private float powerCountPerTick = 1;

    private InputManager _inputManager;
    private float _power;
    private Rigidbody _ballRb;
    private ContactPoint _contact;
    private bool _ballReady;
    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
    }
    
    private void OnEnable()
    {
        InputManager.OnInputManagerSetup += OnInputManagerSetup;
    }

    private void OnDisable()
    {
        InputManager.OnInputManagerSetup -= OnInputManagerSetup;
    }
    
    private void Update()
    {
        if (!_ballReady)
        {
            return;
        }

        if (_inputManager.IsLaunchStarted())
        {
            _animator.SetBool("activate", true);
            if (_power < maxPower)
            {
                _power += powerCountPerTick * Time.deltaTime;
            }
        }

        if (_inputManager.IsLaunchEnded())
        {
            _animator.SetBool("activate", false);
            if (_ballRb == false)
            {
                return;
            }

            _ballRb.AddForce(-1 * _power * _contact.normal, ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        _ballReady = true;
        _power = 0;
        _contact = other.contacts[0];
        _ballRb = other.collider.attachedRigidbody;
    }

    private void OnCollisionExit(Collision other)
    {
        _ballReady = false;
        _ballRb = null;
    }
    
    private void OnInputManagerSetup(InputManager inputManager)
    {
        _inputManager = inputManager;
    }
}
