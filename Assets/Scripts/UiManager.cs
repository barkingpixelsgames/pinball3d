using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public static event Action OnGameReset;
    public static event Action OnBallReset;
    
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI ballsText;
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject resetBallButton;

    private void OnEnable()
    {
        GameManager.OnBallAmountChanged += OnBallAmountChanged;
        GameManager.OnGameOver += OnGameOver;
        ScoreManager.OnScoreChanged += OnScoreChanged;
    }

    private void OnDisable()
    {
        GameManager.OnBallAmountChanged -= OnBallAmountChanged;
        GameManager.OnGameOver -= OnGameOver;
        ScoreManager.OnScoreChanged -= OnScoreChanged;
    }

    public void OnResetButton()
    {
        gameOverPanel.SetActive(false);
        resetBallButton.SetActive(true);
        OnGameReset?.Invoke();
    }
    
    public void OnResetBallButton()
    {
        OnBallReset?.Invoke();
    }

    private void OnBallAmountChanged(int currentBallAmount)
    {
        ballsText.text = "Balls: " + currentBallAmount;
    }
    
    private void OnScoreChanged(int currentScore)
    {
        scoreText.text = "Score: " + currentScore.ToString("D8");
    }
    
    private void OnGameOver()
    {
        gameOverPanel.SetActive(true);
        resetBallButton.SetActive(false);
    }
}
