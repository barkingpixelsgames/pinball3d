
public class NullInput : IInput
{
    public bool IsSidePressed(Sides side) => false;
    public bool IsLaunchStarted() => false;
    public bool IsLaunchEnded() => false;
}
