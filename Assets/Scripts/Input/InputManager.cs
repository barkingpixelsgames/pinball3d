using System;
using UnityEngine;

[RequireComponent(typeof(PlayerInput))]
public class InputManager : MonoBehaviour, IInput
{
    public static event Action<InputManager> OnInputManagerSetup;
    
    private IInput _input;
    private NullInput _nullInput;
    private PlayerInput _playerInput;
    private bool[] _isSideDown = new bool[(int)Sides.Count];
    private bool[] _sideWasPressed = new bool[(int)Sides.Count];

    private void Awake()
    {
        _nullInput = new NullInput();
        _playerInput = GetComponent<PlayerInput>();
        
        _input = _playerInput;
    }

    private void OnEnable()
    {
        UiManager.OnGameReset += OnGameReset;
        GameManager.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        UiManager.OnGameReset -= OnGameReset;
        GameManager.OnGameOver -= OnGameOver;
    }

    private void Start()
    {
        OnInputManagerSetup?.Invoke(this);
    }

    private void Update()
    {
        UpdateIsSideDown(Sides.Left);
        UpdateIsSideDown(Sides.Right);
    }
    
    public bool IsSidePressed(Sides side) => _input.IsSidePressed(side);
    public bool IsLaunchStarted() => _input.IsLaunchStarted();
    public bool IsLaunchEnded() => _input.IsLaunchEnded();
    public bool IsSideDown(Sides side) => _isSideDown[(int)side];

    private void UpdateIsSideDown(Sides side)
    {
        var s = (int)side;

        if (IsSidePressed(side))
        {
            if (!_isSideDown[s] && !_sideWasPressed[s])
            {
                _isSideDown[s] = true;
                _sideWasPressed[s] = true;
            }
            else
            {
                _isSideDown[s] = false;
            }
        }
        else
        {
            _isSideDown[s] = false;
            _sideWasPressed[s] = false;
        }
    }

    private void OnGameReset()
    {
        _input = _playerInput;
    }

    private void OnGameOver()
    {
        _input = _nullInput;
    }
}
