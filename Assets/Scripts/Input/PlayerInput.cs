using UnityEngine;

public class PlayerInput : MonoBehaviour, IInput
{
    public StatefulButton leftSide;
    public StatefulButton rightSide;
    
    public bool IsSidePressed(Sides side)
    {
        var guiSide = side == Sides.Left ? leftSide : rightSide;
        var keyboardSide = side == Sides.Left ? KeyCode.LeftArrow : KeyCode.RightArrow;

        return Input.GetKey(keyboardSide) || guiSide.isPressed;
    }

    public bool IsLaunchStarted()
    {
        return Input.GetKey(KeyCode.Space) ||
               (leftSide.isPressed && rightSide.isPressed);
    }

    public bool IsLaunchEnded()
    {
        return Input.GetKeyUp(KeyCode.Space) ||
               leftSide.isPressEnded || rightSide.isPressEnded;
    }
}
