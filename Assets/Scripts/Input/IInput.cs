
public enum Sides
{
    Left,
    Right,
    Count
}

public interface IInput
{
    bool IsSidePressed(Sides side);
    bool IsLaunchStarted();
    bool IsLaunchEnded();
}
