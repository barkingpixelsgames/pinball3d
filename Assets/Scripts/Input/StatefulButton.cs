using UnityEngine;
using UnityEngine.EventSystems;

public class StatefulButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool isPressStarted { get; private set; }
    public bool isPressed { get; private set; }
    public bool isPressEnded { get; private set; }

    public void OnPointerDown(PointerEventData pointerEventData)
    {
        isPressed = true;
        isPressStarted = true;
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        isPressed = false;
        isPressEnded = true;
    }

    private void Update()
    {
        if (isPressStarted)
        {
            CoroutineUtil.Instance.WaitForEndOfFrame(() => isPressStarted = false);
        }

        if (isPressEnded)
        {
            CoroutineUtil.Instance.WaitForEndOfFrame(() => isPressEnded = false);
        }
    }
}
