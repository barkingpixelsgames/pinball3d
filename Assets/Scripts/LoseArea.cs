using System;
using UnityEngine;

public class LoseArea : MonoBehaviour
{
    public static event Action OnBallLost;
    
    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        OnBallLost?.Invoke();
    }
}
