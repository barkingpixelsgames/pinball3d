using System;
using UnityEngine;

[RequireComponent(typeof(HingeJoint))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(MeshCollider))]
public class Flipper : MonoBehaviour
{
    [SerializeField] private Sides side;
    [SerializeField] private float startPos = 0;
    [SerializeField] private float endPos = 60;
    [SerializeField] private float power = 2500;
    [SerializeField] private float damper = 25f;

    private InputManager _inputManager;
    private HingeJoint _joint;
    private JointSpring _spring;
    private JointLimits _limits;

    private void Awake()
    {
        _joint = GetComponent<HingeJoint>();
        
        // Spring
        _joint.useSpring = true;
        _spring = new JointSpring
        {
            spring = power,
            damper = damper
        };
        _joint.spring = _spring;
        
        // Limits
        _joint.useLimits = true;
        _limits = new JointLimits
        {
            min = startPos,
            max = endPos * GetDirection()
        };
        _joint.limits = _limits;
    }

    private void OnEnable()
    {
        InputManager.OnInputManagerSetup += OnInputManagerSetup;
    }

    private void OnDisable()
    {
        InputManager.OnInputManagerSetup -= OnInputManagerSetup;
    }
    
    private void Update()
    {
        UpdateFlipperPos();
    }

    private void UpdateFlipperPos()
    {
        switch (side)
        {
            case Sides.Left when _inputManager.IsSidePressed(side):
            case Sides.Right when _inputManager.IsSidePressed(side):
                _spring.targetPosition = endPos * GetDirection();
                break;
            default:
                _spring.targetPosition = startPos;
                break;
        }
        _joint.spring = _spring;
    }

    private int GetDirection()
    {
        return side == Sides.Left ? -1 : 1;
    }
    
    private void OnInputManagerSetup(InputManager inputManager)
    {
        _inputManager = inputManager;
    }
}
