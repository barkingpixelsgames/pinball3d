using System;
using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static event Action OnNewBall;
    public static event Action<int> OnBallAmountChanged;
    public static event Action OnGameOver;
    
    [SerializeField] private int startBallAmount = 3;
    [SerializeField] private int multiBallAmount = 3;
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private Transform startSpawnPoint;
    [SerializeField] private Transform multiSpawnPoint;

    private int _currentBallAmount;
    private int _activeBallsOnTable;
    // Limit FPS
    private int _targetFPS = 60;
    private int _backgroundFPS = 15;
    //

    private void Awake()
    {
        // Limit FPS
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = _targetFPS;
        //
        
        OnGameReset();
    }

    private void OnEnable()
    {
        LoseArea.OnBallLost += OnBallLost;
        Mission.OnMultiballActivate += OnMultiballActivate;
        UiManager.OnGameReset += OnGameReset;
        UiManager.OnBallReset += OnBallReset;
    }

    private void OnDisable()
    {
        LoseArea.OnBallLost -= OnBallLost;
        Mission.OnMultiballActivate -= OnMultiballActivate;
        UiManager.OnGameReset -= OnGameReset;
        UiManager.OnBallReset -= OnBallReset;
    }
    
    private void Update() {
        // Limit FPS
        if (Application.isFocused) {
            if (Application.targetFrameRate != _targetFPS) {
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = _targetFPS;
            }
        }
        else {
            if (Application.targetFrameRate != _backgroundFPS) {
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = _backgroundFPS;
            }
        }
        //
    }

    private void OnGameReset()
    {
        _currentBallAmount = startBallAmount;
        CreateNewBall();
    }

    private void OnBallReset()
    {
        if (_activeBallsOnTable != 1)
        {
            return;
        }

        GameObject.FindWithTag("Ball").transform.position = startSpawnPoint.position;
    }

    private void OnBallLost()
    {
        _activeBallsOnTable--;
        if (_activeBallsOnTable == 0 && _currentBallAmount > 0)
        {
            CreateNewBall();
            OnNewBall?.Invoke();
        }
        else
        {
            // Game Over
            OnGameOver?.Invoke();
        }
    }

    private void CreateNewBall()
    {
        Instantiate(ballPrefab, startSpawnPoint.position, Quaternion.identity);
        _activeBallsOnTable++;
        _currentBallAmount--;
        OnBallAmountChanged?.Invoke(_currentBallAmount);
    }

    private void OnMultiballActivate(int missionId)
    {
        StartCoroutine(StartMultiball());
    }

    private IEnumerator StartMultiball()
    {
        var amount = multiBallAmount;
        while (amount > 0)
        {
            Instantiate(ballPrefab, multiSpawnPoint.position, Quaternion.identity);
            _activeBallsOnTable++;
            amount--;
            yield return new WaitForSeconds(1f);
        }
    }
}
