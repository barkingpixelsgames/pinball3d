using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MissionManager))]
public class MissionListEditor : Editor
{
    private MissionManager _manager;
    private SerializedObject _getTarget;
    private SerializedProperty _theList;
    private int _listSize;
    private readonly List<bool> _newShowFoldout = new List<bool>();

    private void OnEnable()
    {
        _manager = (MissionManager)target;
        _getTarget = new SerializedObject(_manager);
        _theList = _getTarget.FindProperty("missionList");
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        _getTarget.Update();
        
        // List size
        EditorGUILayout.LabelField("Mission List Size");
        _listSize = _theList.arraySize;
        _listSize = EditorGUILayout.IntField("List Size", _listSize);
        if (_listSize != _theList.arraySize)
        {
            while (_listSize > _theList.arraySize)
            {
                _theList.InsertArrayElementAtIndex(_theList.arraySize);
            }
            while (_listSize < _theList.arraySize)
            {
                _theList.DeleteArrayElementAtIndex(_theList.arraySize - 1);
            }
        }
        
        // Show foldout
        while (_newShowFoldout.Count > _theList.arraySize)
        {
            _newShowFoldout.RemoveAt(_newShowFoldout.Count - 1);
        }
        while (_newShowFoldout.Count < _theList.arraySize)
        {
            _newShowFoldout.Add(true);
        }

        // Add mission button
        if (GUILayout.Button("Add New Mission"))
        {
            _manager.GetMissionList.Add(new Mission());
        }
        
        // Display content in every foldout
        for (int i = 0; i < _theList.arraySize; i++)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginVertical(GUI.skin.box);
            _newShowFoldout[i] = EditorGUILayout.Foldout(_newShowFoldout[i], "Mission " + (i + 1));
            
            SerializedProperty myListRef = _theList.GetArrayElementAtIndex(i);
            SerializedProperty myId = myListRef.FindPropertyRelative("missionId");
            SerializedProperty myDescription = myListRef.FindPropertyRelative("description");
            SerializedProperty myIsActiveFromStart = myListRef.FindPropertyRelative("isActiveFromStart");
            SerializedProperty myCanTriggerMultiball = myListRef.FindPropertyRelative("canTriggerMultiball");
            SerializedProperty myResetOnNextBall = myListRef.FindPropertyRelative("resetOnNextBall");
            SerializedProperty myStopOnBallEnd = myListRef.FindPropertyRelative("stopOnBallEnd");
            SerializedProperty myResetOnComplete = myListRef.FindPropertyRelative("resetOnComplete");
            SerializedProperty myTimeToComplete = myListRef.FindPropertyRelative("timeToComplete");
            SerializedProperty myScore = myListRef.FindPropertyRelative("score");
            SerializedProperty myAmountToComplete = myListRef.FindPropertyRelative("amountToComplete");
            SerializedProperty myLightShow = myListRef.FindPropertyRelative("lightShow");
            
            // Show content
            if (_newShowFoldout[i])
            {
                EditorGUILayout.PropertyField(myId);
                EditorGUILayout.PropertyField(myDescription);
                EditorGUILayout.PropertyField(myIsActiveFromStart);
                EditorGUILayout.PropertyField(myCanTriggerMultiball);
                EditorGUILayout.PropertyField(myResetOnNextBall);
                EditorGUILayout.PropertyField(myStopOnBallEnd);
                EditorGUILayout.PropertyField(myResetOnComplete);
                EditorGUILayout.PropertyField(myTimeToComplete);
                EditorGUILayout.PropertyField(myScore);
                EditorGUILayout.PropertyField(myAmountToComplete);
                EditorGUILayout.PropertyField(myLightShow);
                
                if (myResetOnNextBall.boolValue && myStopOnBallEnd.boolValue)
                {
                    myResetOnNextBall.boolValue = myStopOnBallEnd.boolValue = false;
                }
            }

            // Remove mission button
            if (GUILayout.Button("Delete Mission"))
            {
                _theList.DeleteArrayElementAtIndex(i);
            }

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();
            EditorGUILayout.Separator();
        }

        _getTarget.ApplyModifiedProperties();
    }
}
