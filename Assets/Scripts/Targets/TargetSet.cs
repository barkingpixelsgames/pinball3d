using System.Collections;
using UnityEngine;

public class TargetSet : MonoBehaviour
{
    private Target[] _targets;
    private bool _resetCheck;
    private int _missionId;

    private void OnEnable()
    {
        _targets = GetComponentsInChildren<Target>();
        _missionId = GetComponentInChildren<MissionSensor>().GetMissionIdToTrigger;
        MissionManager.OnMissionActiveFromStart += OnMissionActiveFromStart;
        Mission.OnMissionReset += OnMissionReset;
        Mission.OnMissionDeactivate += OnMissionDeactivate;
    }

    private void OnDisable()
    {
        MissionManager.OnMissionActiveFromStart -= OnMissionActiveFromStart;
        Mission.OnMissionReset -= OnMissionReset;
        Mission.OnMissionDeactivate -= OnMissionDeactivate;
    }

    private void OnMissionActiveFromStart(int missionId)
    {
        if (_missionId == missionId)
        {
            StartCoroutine(ActivateTargets(true, 0f));
        }
    }
    
    private void OnMissionReset(int missionId)
    {
        if (_missionId == missionId)
        {
            StartCoroutine(ActivateTargets(true, 0.3f));
        }
    }
    
    private void OnMissionDeactivate(int missionId)
    {
        if (_missionId == missionId)
        {
            StartCoroutine(ActivateTargets(false, 0f));
        }
    }

    private IEnumerator ActivateTargets(bool activate, float delay)
    {
        if (_resetCheck)
        {
            yield break;
        }

        _resetCheck = true;
        yield return new WaitForSeconds(delay);
        ActivateAllTargets(activate);

        _resetCheck = false;
    }

    private void ActivateAllTargets(bool activate)
    {
        foreach (var target in _targets)
        {
            target.ActivateTarget(activate);
        }
    }
}
