using UnityEngine;

public class Target : MonoBehaviour
{
    private Animator _animator;
    private Collider _collider;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _collider = GetComponent<Collider>();

        ActivateTarget(false);
    }

    public void ActivateTarget(bool isOn)
    {
        if (_animator)
        {
            _collider.enabled = isOn;
            _animator.SetBool("activated", isOn);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        ActivateTarget(false);
    }
}
