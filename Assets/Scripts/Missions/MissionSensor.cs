using System;
using UnityEngine;

public class MissionSensor : MonoBehaviour
{
    public static event Action<int> OnMissionStart;
    public static event Action<int> OnMissionUpdate;
    
    [SerializeField] private bool isStarterSensor;
    [SerializeField] private bool isCounterSensor;
    [Space]
    [SerializeField] private int missionIdToTrigger;
    [Header("Debug")]
    [SerializeField] private bool showGizmos = true;

    public int GetMissionIdToTrigger => missionIdToTrigger;

    private void OnTriggerEnter(Collider other)
    {
        if (isStarterSensor)
        {
            // Start the mission -> Mission Manager
            OnMissionStart?.Invoke(missionIdToTrigger);
        }

        if (isCounterSensor)
        {
            // Update the mission -> Mission Manager
            OnMissionUpdate?.Invoke(missionIdToTrigger);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (isStarterSensor)
        {
            // Start the mission -> Mission Manager
            OnMissionStart?.Invoke(missionIdToTrigger);
        }

        if (isCounterSensor)
        {
            // Update the mission -> Mission Manager
            OnMissionUpdate?.Invoke(missionIdToTrigger);
        }
    }

    private void OnDrawGizmos()
    {
        if (showGizmos)
        {
            Gizmos.color = new Color32(0, 0, 255, 125);
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.DrawCube(Vector3.zero, Vector3.one);
        }
    }
}
