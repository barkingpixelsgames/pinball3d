using System;
using System.Collections;
using UnityEngine;

public enum ResetModes
{
    OnComplete,
    OnNextBall,
    OnGameReset
}

[Serializable]
public class Mission
{
    public static event Action<int> OnMissionReset;
    public static event Action<int> OnMissionDeactivate;
    public static event Action<int> OnMultiballActivate;
    public static event Action<int, int> OnMissionComplete;
    
    [SerializeField] private int missionId;
    [SerializeField] private string description;
    [Space]
    [SerializeField] private bool isActiveFromStart;
    [SerializeField] private bool canTriggerMultiball;
    [Space]
    [SerializeField] private bool resetOnNextBall;
    [SerializeField] private bool stopOnBallEnd;
    [SerializeField] private bool resetOnComplete;
    [Space]
    [SerializeField] private float timeToComplete;
    [Space]
    [SerializeField] private int score;
    [SerializeField] private int amountToComplete;
    [Space]
    [SerializeField] private LightShow lightShow;

    private bool _isActive;
    private bool _isCompleted;
    private int _currentAmount;
    private bool _isTimeBasedMissionActive;

    public int GetMissionId => missionId;
    public bool IsActiveFromStart => isActiveFromStart;
    public bool StopOnBallEnd => stopOnBallEnd;
    public float GetTimeToComplete => timeToComplete;
    public LightShow GetLightShow => lightShow;
    
    public bool IsActive
    {
        get => _isActive;
        set => _isActive = value;
    }
    public bool IsCompleted
    {
        get => _isCompleted;
        set => _isCompleted = value;
    }
    public bool IsTimeBasedMissionActive
    {
        get => _isTimeBasedMissionActive;
        set => _isTimeBasedMissionActive = value;
    }

    public void ResetMission(ResetModes mode)
    {
        StopMission();

        if (mode == ResetModes.OnComplete && resetOnComplete ||
            mode == ResetModes.OnNextBall && resetOnNextBall ||
            mode == ResetModes.OnGameReset)
        {
            _isCompleted = false;
            OnMissionReset?.Invoke(missionId);
        }
    }

    public void DeactivateMission()
    {
        StopMission();
        OnMissionDeactivate?.Invoke(missionId);
    }

    private void StopMission()
    {
        _isActive = false;
        _currentAmount = 0;
        
        if (timeToComplete > 0)
        {
            // Stop the timer
            _isTimeBasedMissionActive = false;
        }
        
        if (lightShow)
        {
            lightShow.StopLightShow();
        }
    }

    public void UpdateMission()
    {
        if (_isActive && !_isCompleted)
        {
            _currentAmount++;
            
            // Check if the mission is complete
            CheckMissionComplete();
        }
    }

    public void StartMissionTimer(MissionManager manager)
    {
        manager.StartCoroutine(MissionTimer());
    }

    private void CheckMissionComplete()
    {
        if (_currentAmount >= amountToComplete)
        {
            _isCompleted = true;

            if (canTriggerMultiball)
            {
                OnMultiballActivate?.Invoke(missionId);
            }

            OnMissionComplete?.Invoke(missionId, score);
            ResetMission(ResetModes.OnComplete);
        }
    }
    
    private IEnumerator MissionTimer()
    {
        var tempTime = timeToComplete;
        while (_isTimeBasedMissionActive && tempTime > 0)
        {
            yield return new WaitForSeconds(1f);
            tempTime -= 1;
        }
        
        // Deactivate mission
        DeactivateMission();
    }
}
