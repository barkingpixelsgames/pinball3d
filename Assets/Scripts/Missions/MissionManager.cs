using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MissionManager : MonoBehaviour
{
    public static event Action<int> OnMissionActiveFromStart;
    
    [SerializeField] private List<Mission> missionList = new List<Mission>();

    private bool _isMissionStart;

    public List<Mission> GetMissionList => missionList;

    private void OnEnable()
    {
        MissionSensor.OnMissionStart += OnMissionStart;
        MissionSensor.OnMissionUpdate += OnMissionUpdate;
        Mission.OnMissionReset += OnMissionReset;
        GameManager.OnNewBall += OnNewBall;
        UiManager.OnGameReset += OnGameReset;
    }

    private void OnDisable()
    {
        MissionSensor.OnMissionStart -= OnMissionStart;
        MissionSensor.OnMissionUpdate -= OnMissionUpdate;
        Mission.OnMissionReset -= OnMissionReset;
        GameManager.OnNewBall -= OnNewBall;
        UiManager.OnGameReset -= OnGameReset;
    }
    
    private void Start()
    {
        // If mission is active from start, activate mission
        foreach (var mission in missionList.Where(mission => mission.IsActiveFromStart))
        {
            StartMission(mission, mission.GetTimeToComplete > 0);
            OnMissionActiveFromStart?.Invoke(mission.GetMissionId);
        }
    }

    private void OnMissionStart(int missionId)
    {
        foreach (var mission in missionList.Where(mission => mission.GetMissionId == missionId))
        {
            _isMissionStart = false;
                
            // Time based missions
            if (!mission.IsCompleted && !mission.IsActive &&
                mission.GetTimeToComplete > 0 && !mission.IsTimeBasedMissionActive)
            {
                _isMissionStart = true;
                StartMission(mission, true);
            }
            else if(!mission.IsCompleted && !mission.IsActive &&
                    mission.GetTimeToComplete <= 0)
            {
                _isMissionStart = true;
                StartMission(mission, false);
            }
        }
    }

    private void StartMission(Mission mission, bool isTimeBased)
    {
        mission.IsActive = true;

        // Start LightShow
        if (mission.GetLightShow)
        {
            mission.GetLightShow.StartLightShow();
        }

        // Start timer
        if (isTimeBased)
        {
            mission.IsTimeBasedMissionActive = true;
            mission.StartMissionTimer(this);
        }
    }

    private void OnMissionUpdate(int missionId)
    {
        if (_isMissionStart)
        {
            return;
        }
        
        missionList.Find(m => m.GetMissionId == missionId).UpdateMission();
    }

    private void OnMissionReset(int missionId)
    {
        foreach (var mission in missionList.Where(mission => mission.GetMissionId == missionId))
        {
            if (mission.IsActiveFromStart)
            {
                StartMission(mission, mission.GetTimeToComplete > 0);
            }
        }
    }

    private void OnNewBall()
    {
        foreach (var mission in missionList.Where(mission => mission.IsActive))
        {
            if (mission.StopOnBallEnd)
            {
                mission.DeactivateMission();
                return;
            }
            
            mission.ResetMission(ResetModes.OnNextBall);
        }
    }

    private void OnGameReset()
    {
        foreach (var mission in missionList.Where(mission => mission.IsActive))
        {
            mission.ResetMission(ResetModes.OnGameReset);
        }
    }
}
